Summary:       A mirror of bpf-next Linux source tree's tools/lib/bpf directory
Name:          libbpf
Version:       1.2.2
Release:       5%{?dist}
License:       LGPL-2.1 or BSD-2-Clause
URL:           https://github.com/%{name}/%{name}
Source:        https://github.com/%{name}/%{name}/archive/v%{version}.tar.gz #/%{name}-%{version}.tar.gz

Patch3000:	3000-libbpf-1.2.2-Fix-loongarch-upstream-code.patch

BuildRequires: gcc make elfutils-libelf-devel elfutils-devel

%description
This is a mirror of bpf-next Linux source tree's tools/lib/bpf directory plus its 
supporting header files.

%package static
Summary: Static library for libbpf development
Requires: %{name}-devel = %{version}-%{release}

%description static
This package contains static library for developing applications
that use %{name}

%package devel
Summary:        Supporting header files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kernel-headers >= 5.16.0
Requires:       zlib

%description devel
This package contains libraries and header files for developing 
applications that use %{name}

%prep
%autosetup -n %{name}-%{version} -p1

%build
pushd src
%make_build NO_PKG_CONFIG=1
popd

%install
pushd src
%make_install
popd

%files
%{_libdir}/libbpf.so.%{version}
%{_libdir}/libbpf.so.1

%files static
%{_libdir}/libbpf.a

%files devel
%{_includedir}/bpf/
%{_libdir}/pkgconfig/libbpf.pc
%{_libdir}/libbpf.so

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.2.2-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.2.2-4
- Rebuilt for loongarch release

* Tue Jul 16 2024 Huang Yang <huangyang@loongson.cn> - 1.2.2-3
- [Type] other
- [DESC] Fix loongarch upstream code

* Wed Jan 17 2024 Rebuild Robot <rebot@opencloudos.org> - 1.2.2-2
- Rebuilt for elfutils

* Thu Jan 11 2024 Upgrade Robot <upbot@opencloudos.org> - 1.2.2-1
- Upgrade to version 1.2.2

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.2.0-2
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Jul 13 2023 kianli <kianli@tencent.com> - 1.2.0-1
- Upgrade to 1.2.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.8.0-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.8.0-3
- Rebuilt for OpenCloudOS Stream 23

* Thu Feb 16 2023 rockerzhu <rockerzhu@tencent.com> - 0.8.0-2
- Rebuild for kernel 6.1

* Fri Jun 24 2022 Xiaojie Chen <jackxjchen@tencent.com> - 0.8.0-1
- Initial build
